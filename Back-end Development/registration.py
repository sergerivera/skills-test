class Person():
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
    
    def sleep(self):
        return self.first_name + " " + self.last_name + " is now sleeping."

class Student(Person):
    def __init__(self, first_name, last_name, student_number):
        super().__init__(first_name, last_name)
        self.student_number = student_number
        self.isRegistered = False
    
    def sleep(self):
        return "Student " + self.student_number + " is now sleeping."

class Administrator(Person):
    def register(self, student):
        if (student.isRegistered):
            return "Student " + student.student_number + " is already registered."
        else: 
            student.isRegistered = True
            return "Student " + student.student_number + " is registered."

student = Student("First Name", "Last Name", "1234567")
admin = Administrator("UP", "CRS")
print(student.sleep())
print(admin.sleep())
print(admin.register(student))
print(admin.register(student))