const form = document.querySelector("form");
const formWrapper = document.getElementsByClassName("form-wrapper")[0];
const statusBox = document.getElementsByClassName("status")[0];

form.onsubmit = function(event){
    event.preventDefault();
    const user = document.getElementById("user");
    const pass = document.getElementById("pass");
    const remember = document.getElementById("remember");
    statusBox.style.visibility = "visible";
    if (user.value == "serge" && pass.value == "123"){
        statusBox.innerHTML = "Successfully logged in";
        statusBox.style.backgroundColor = "lightgreen";
    }
    else{
        statusBox.innerHTML = "Invalid login details";
        statusBox.style.backgroundColor = "pink";
    }
}