// declaration of variables
var light = document.getElementById("light");
var dark = document.getElementById("dark");
var body = document.querySelector("body");

// change styles upon pressing of buttons
light.addEventListener("click", changeLight);
dark.addEventListener("click", changeDark);

// function definitions
function changeLight(){
    body.setAttribute("style", "background-color: white; color: black");
}

function changeDark(){
    body.setAttribute("style", "background-color: black; color: white");
}

/* light.addEventListener("click", function(){
    body.setAttribute("style", "background-color: white; color: black");
});

dark.addEventListener("click", function(){
    body.setAttribute("style", "background-color: black; color: white");
}); */

/* light.onclick = function(){
    body.setAttribute("class", "light");
}

dark.onclick = function(){
    body.setAttribute("class", "dark");
} */

function add(x, y){
    return x + y;
}

var variable1 = 210;
var variable2 = 210;
var variable3 = add(variable1, variable2);
console.log(variable3);

var counter = 0;
while (counter < 9){
    counter = counter + 1;
    console.log(counter);
}

var i = 1;
for (; i < 10; i++){
    console.log(i)
}